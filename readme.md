# fat_sat_tools

Tools for FAT:s and SAT:s using pyads and TwinCAT

## Edit configuration file

Open the **cfgTests.ini** file and edit it accordingly.

Specify the Ams NetID of your motion controller and the index of the GVL array of the axis to be tested.

Do **NOT** change the value of ***TESTNUM_VAR***

Write a ***1*** to enable the tests to be performed.

Specify all the parameters needed for the tests.

## cfgTests.ini example

    [General Configuration]
    PLC_NETID = 172.30.41.144.1.1           # AMS NetID of the motion controller
    MOTOR_GVL_INDEX = 1                     # Index of the motor used in the GVL.astAxes array (control)
    TESTNUM_VAR = "MAIN.nTestNum"           # Test number variable (control and report)
    VELO = 10                               # Velocity setpoint (Write 0 to use the TwinCAT NC velocities)
    TIMEOUT = 200                           # Maximum time in seconds from one limit swtich to the other

    [ISO230-2 Configuration]
    ISO230_ENABLE = 0                       # Enable ISO230-2 test cycle (control and report)
    ISO230_FIRST_TEST_POS = 15              # First test position (control)
    ISO230_POS_STEP = 10.2                  # Step between test positions (control)
    ISO230_POS_COUNT = 5                    # Position count (control and report)
    ISO230_CYCLE_COUNT = 5                  # Test cycle count (control and report)
    ISO230_START_POS = 10                   # Approach first test position from below (control)
    ISO230_END_POS = 60                     # Approach last test from above when backward (control)

    [Resolver Configuration]
    RESOLVER_ENABLE = 0                     # Enable resolver performance test (control and report)
    RESOLVER_TEST_POS = 35                  # Start resolver test at this position (control)
    RESOLVER_TEST_STEP = 0.125              # Increment between resolver tests (control)

    [Limits Configuration]
    LIMITS_ENABLE = 1                       # Enable test of limit switches (control and report)

    [Scan Configuration]
    SCAN_ENABLE = 1                         # Enable scan (low limit to high limit to low limit) (control and report)


## Start data logging example
For logging:
Clone the Gitlab repository called ***py-ads-monitor***

usage: adsmonitor.py [options] PlcVariable [ PlcVariable ... ]

usage:   %prog [options] PlcVariable [ PlcVariable ... ]\n\
example: python adsmonitor.py --target-netid=192.168.88.60.1.1 --target-ams-port=852 [options] PlcVariable [ PlcVariable ... ]\n"

**Options:**
* "--local-time": Use local time instead of PLC time
* "--numeric": Print boolean as numeric
* "--target-netid": The AMS Net ID of the target
* "--target-ams-port": Target AMS port
* "-v, --verbose": Verbosity level (default: %default)

python adsmonitor.py --target-netid=172.18.21.41.1.1 --target-ams-port=852 --numeric GVL.astAxes[1].Axis.NcToPlc.ActPos|tee Logfile1.log


## Start test sequence
All scripts and code for testing using TwinCAT are in the ***TwinCAT*** sub dir.

Note: All files in this dir starting with "ctrl*" are for handling control of the test sequences.

Once the ***cfgTests.ini*** file is configured run the python script ***ctrlRunTests.py***.

    cd TwinCAT
    python ctrlRunTests.py


## Generate report

All scripts and code for report generation and data analysis are in the ***report*** sub dir.

Make sure that all data of the ***reportTC.cfg*** file is correct.


## reportTC.cfg example

    ############# General config:
    TESTNUM_PV="nTestNum"               # Test number variable name
    MOTORSET_PV="SetPos"                # Target position data source variable
    MOTORACT_PV="ActPos"                # Position actual data source variable (not transformed by gear ratio calculations)
    REFERENCE_PV="nEncoderRawCounter"   # Reference position data source variable
    DEC=5                               # Number of decimals in printouts
    UNIT="mm"                           # Units for printouts
    ############# ISO230-2 config:
    ISO230_ENABLE=1                     # Enable ISO230-2 test cycle
    ISO230_POS_COUNT=5                  # Position count
    ISO230_CYCLE_COUNT=5                # Test cycle count
    ISO230_REF_POS_PV=$REFERENCE_PV     # Reference positon variable
    ############# Resolver config:
    RESOLVER_ENABLE=0                   # Enable resolver performance test
    RESOLVER_CALC_GR=0                  # Needed if resolver Enable or if resolver is used as source for limits calcs
    RESOLVER_PV="nEncoderRawCounter"    # Resolver data source variable
    ############# Limits config:
    LIMITS_ENABLE=1                     # Enable test of limit switches
    LIMITS_LOW_PV="bLimitBwd"           # Data source variable for bwd limit switch
    LIMITS_HIGH_PV="bLimitFwd"          # Data source variable for fwd limit switch
    LIMITS_REF_POS_PV=$MOTORACT_PV      # Position data reference source variable
    ############# Scan config:
    SCAN_ENABLE=1                       # Enable scan (fwd limit to bwd limit to fwd limit)


Once the ***reportTC.cfg*** file has the proper variable names run the bash code to create the report

    cd report
    bash reportMain.bash ../TwinCAT/reportTC.cfg ../TwinCAT/Data_log/LogFile.log ../TwinCAT/TCreport.md


## Test number coding

### Limits

    Lowlimit engage: 3001..3010
    Lowlimit disengage 3011.3020.
    Lowlimit engage: 5001..5010
    Lowlimit disengage 5011.5020.


### Resolver

    Resolver: 4001..4008

### Fwd to Bwd to Fwd limit Scan

    Scan: 6000


### ISO230

    ISO230: <dir><cycle><position index two digits>
    Forward dir: 1
    Backward dir: 2

    The sequence will be like this (for 5 positions):
    1101    (forward, cycle 1, position 01)
    1102
    1103
    1104
    1105
    2105
    2104
    2103
    2102
    2101
    1201
    1202
    1203
    1204
    1205
    2205 (backward, cycle 2, position 02)
    2204
    ...
    ...
    ...
    1505
    2505
    2504
    2503
    2502
    2501 (backward, cycle 5 position 01)
