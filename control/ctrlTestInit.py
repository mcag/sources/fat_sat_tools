#!/usr/bin/env python

import configparser
from motionFunctionsLib import *
import os
import time
import sys
sys.path.append('./TwinCAT/pyads-motion-library')
import motionFunctionsLib


########Internal variables########
defaultPLCport = 852
sleepTime = 1
##################################

if len(sys.argv)<3 or len(sys.argv)>3:
  print('Error: Wrong number of arguments')
  print("How to call: python ctrlTestInit.py <plcNetId> <gvlAxisIndex> <plcPort=852>")
  print("Call example: python ctrlTestInit.py 172.30.41.144.1.1 1 852")
  sys.exit()

try:
  plcNetId = sys.argv[1]
except:
  print('Error: Please specify the NetId of the motion controller')
  print("How to call: python ctrlTestInit.py <plcNetId> <gvlAxisIndex> <plcPort=852>")
  print("Call example: python ctrlTestInit.py 172.30.41.144.1.1 1 852")
  sys.exit()
try:
  gvlAxisIndex = int(sys.argv[2])
except:
  print("ERROR: Please specify the MOTOR_GVL_INDEX")
  print("How to call: python ctrlTestInit.py <plcNetId> <gvlAxisIndex> <plcPort=852>")
  print("Call example: python ctrlTestInit.py 172.30.41.144.1.1 1 852")
  sys.exit()
try:
  plcPort = int(sys.argv[3])
except:
  plcPort = defaultPLCport #Default PLC port for the tc_project_App is 852  

print(f"Connecting to {plcNetId} at port {plcPort}")

#PLC connection
plc1 = plc(plcNetId, plcPort)
plc1.connect()

if not plc1.connection.is_open:
  print(f"Error: Connection to PLC at {plcNetId} at port {plcPort} failed..")  

plc1.connection.write_by_name('MAIN.nTestNum', 0)

time.sleep(0.5)
if plc1.connection.read_by_name('MAIN.nTestNum') == 0:
  print ('    init done')
else:
  print ('    init failed')


#Set initial conditions
axis1 = axis(plc1, gvlAxisIndex)
axis1.axisInit()

#Check if homed    
if not axis1.getHomedStatus():
  axis1.home()
  if axis1.waitForCommandDone() and axis1.waitForStatusBit(axis1.getHomedStatus, True):
    print (f'   Axis {gvlAxisIndex} homed...')
    time.sleep(sleepTime)
  else:
    print (f'   Axis {gvlAxisIndex} homing failed...')
    sys.exit()
else:
  print (f'   Axis {gvlAxisIndex} homed...')
