#!/usr/bin/env python

import time
import sys
from motionFunctionsLib import *

########Internal variables########
counter = 0
testNumberBase = 4000
defaultPLCport = 852
defaultNumOfSteps = 8
sleepTime = 1
##################################

if len(sys.argv)<5:
  print("ERROR: Missing arguments")  
  print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <stepSize> <steps=8> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 10.6 8 2 852")
  sys.exit()

try:
  plcNetId = sys.argv[1]
except:
  print('Error: Please specify the NetId of the motion controller')
  print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <stepSize> <steps=8> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 10.6 8 2 852")
  sys.exit()
try:
    axisIdx = sys.argv[2]
except:
  print('Error: Please specify the Axis Index of the axis to test')
  print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <stepSize> <steps=8> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 10.6 8 2 852")
  sys.exit()
try:
  stepSize = float(sys.argv[3])
  if stepSize <= 0:
    print("Error: Step size has to be greater than 0")
    sys.exit() 
except:
  print('Error: Please specify the step size of the test')
  print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <stepSize> <steps=8> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 10.6 8 2 852")
  sys.exit()
try:
  steps = int(sys.argv[5])
  if steps <= 0:
    print("Error: Number of steps have to be more than 0")
    sys.exit()
except:
  print(f'Using default number of steps: {defaultNumOfSteps} ')
  steps = None
try:
  velo = float(sys.argv[4])
  if velo <= 0:
        print(f"Using TwinCAT NC configured velocity")
        velo = None
except:
  velo = None
try:
  plcPort = int(sys.argv[6])
except:
  plcPort = defaultPLCport #Default PLC port for the tc_project_App is 852

#PLC connection
plc1 = plc(plcNetId, plcPort)
axis1 = axis(plc1, axisIdx)

if not plc1.connection.is_open:
    for i in range(3):
        plc1.connect()
        time.sleep(sleepTime)
        i += 1
        if i == 3:
             print('  ERROR: PLC cannot connect')
             sys.exit()
        elif plc1.connection.is_open:
            print('   PLC connection successful')
            break

########Set initial conditions#######
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase)
startPos = axis1.getActPos()+1

#Checking if Axis homed    
if not axis1.getHomedStatus():
    print(f"    GVL.astAxes[{axisIdx}] not homed.")
    print("     Starting homing...")
    axis1.home()
    if axis1.waitForStatusBit(axis1.getHomedStatus, True):
        print (f'   Axis {axisIdx} homed...')
        time.sleep(sleepTime)
else:
  print("   Axis homed")
   
axis1.axisInit() #Disable, reset, enable

if steps is not None:
  defaultNumOfSteps = steps

while counter < defaultNumOfSteps:
  print (f'    move axis to position {startPos+counter*stepSize} (cycles = {counter})...')  
  if axis1.moveAbsoluteAndWait(startPos+counter*stepSize):
    counter = counter + 1
  else:
    print (f"    ERROR: motor failed to position")
    sys.exit()
  time.sleep(sleepTime)
  plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase+counter)

time.sleep(sleepTime)
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase) 
print ('    Test Finished')
