#!/usr/bin/env python

import time
import sys
from motionFunctionsLib import *

########Internal variables########
sleepTime = 1
##################################

if len(sys.argv)<4:
  print("ERROR: Missing arguments: python ctrlTestMovePos.py <plcNetId> <axisIndex> <position> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 2 5 852")
  sys.exit()

try:
    plcNetId = sys.argv[1]
except:
    print('Error: Please specify the NetId of the motion controller')
    print("ERROR: Missing arguments: python ctrlTestMovePos.py <plcNetId> <axisIndex> <position> <velo=NC_default_vels> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 2 5 852")
    sys.exit()
try:
    axisIdx = sys.argv[2]
except:
    print('Error: Please specify the Axis Index of the axis to test')
    print("ERROR: Missing arguments: python ctrlTestMovePos.py <plcNetId> <axisIndex> <position> <velo=NC_default_vels> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 2 5 852")
    sys.exit()
try:
  position = float(sys.argv[3])
except:
    print('Error: Please specify the target position')
    print("ERROR: Missing arguments: python ctrlTestMovePos.py <plcNetId> <axisIndex> <position> <velo=NC_default_vels> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 2 5 852")
    sys.exit()
try:
    velo = float(sys.argv[4])
    if velo <= 0:
        print(f"Using TwinCAT NC configured velocity")
        velo = None
except:
    velo = None 
try:
    plcPort = int(sys.argv[5])
except:
    plcPort = 852 #Default PLC port for the tc_project_App is 852

print(f"  Connecting to {plcNetId} at port {plcPort} to GVL.astAxes[{axisIdx}]")

#PLC connection
plc1 = plc(plcNetId, plcPort)
axis1 = axis(plc1, axisIdx)

if not plc1.connection.is_open:
    for i in range(3):
        plc1.connect()
        time.sleep(sleepTime)
        i += 1
        if i == 3:
             print('    ERROR: PLC cannot connect')
             sys.exit()
        elif plc1.connection.is_open:
            print('   PLC connection successful')
            break

########Set velocity#######
if velo is not None: 
    axis1.setVelocity(velo)
time.sleep(sleepTime)

#######Moving to position
print(f'    Move axis to position: {position}...')
if axis1.moveAbsoluteAndWait(position):

  print(f'    Move done, axis in position: {axis1.getActPos()}')
else:
  print(f"  ERROR: Axis timeout did not reach position {position}")
  sys.exit()
