#!/usr/bin/env python

import time
import sys
from motionFunctionsLib import *

########Internal variables########
testLoops = 10
counter = 0
testNumberBase = 3000
defaultStepsize = 5
defaultPLCport = 852
sleepTime = 1
##################################

if len(sys.argv)<4:
  print("ERROR: Missing arguments: python ctrlTestLimitBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <stepSize=3> <plcPort=852>")
  print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 100 2 5 852")
  sys.exit()

try:
    plcNetId = sys.argv[1]
except:
    print('Error: Please specify the NetId of the motion controller')
    print("python ctrlTestLimitBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <stepSize=3> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 100 2 5 852")
    sys.exit()
try:
    axisIdx = sys.argv[2]
except:
    print('Error: Please specify the Axis Index of the axis to test')
    print("python ctrlTestLimitBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <stepSize=3> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 100 2 5 852")
    sys.exit()
try:
    timeout = int(sys.argv[3])
    if timeout <= 0:
       print('Error: Timeout value should be greater than 0')
       sys.exit()  
except:
    print('Error: Please specify a timeout value') 
    print("python ctrlTestLimitBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <stepSize=3> <plcPort=852>")
    print("python ctrlTestLimitBwd.py 172.30.41.144.1.1 1 100 2 5 852")
    sys.exit()
try:
    velo = float(sys.argv[4])
    if velo <= 0:
        print(f"Using TwinCAT NC configured velocity")
        velo = None
except:
    velo = None 
try:
    stepSize = float(sys.argv[5])
except:
    stepSize = defaultStepsize #Default value for the realtive move after hitting the switch
try:
    plcPort = int(sys.argv[6])
except:
    plcPort = defaultPLCport #Default PLC port for the tc_project_App is 852



print(f"Connecting to {plcNetId} at port {plcPort} to GVL.astAxes[{axisIdx}]")

#PLC connection
plc1 = plc(plcNetId, plcPort)
axis1 = axis(plc1, axisIdx)

if not plc1.connection.is_open:
    for i in range(3):
        plc1.connect()
        time.sleep(sleepTime)
        i += 1
        if i == 3:
             print('    ERROR: PLC cannot connect')
             sys.exit()
        elif plc1.connection.is_open:
            print('PLC connection successful')
            break

########Set initial conditions#######
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase)

axis1.axisInit() #Disable, reset, enable
bwdSoftLimitState = axis1.getSoftLimitBwdEnableStatus()

#Starting the test
print('     Initializing test prerequisites...')

########Set velocity#######
if velo is not None: 
    axis1.setJogVelocity(velo)
time.sleep(sleepTime)

print('     Moving to Bwd Limit swtich...')
axis1.moveToSwitchBwd(timeout)

switchPosition = axis1.getActPos() #use to calculate the internal timeout
time.sleep(sleepTime)

print('     Moving to stepsize starting position...')
if axis1.moveRelativeAndWait(abs(stepSize)):
   print('    Test ready to begin')


time.sleep(1)
# Starting the engage limit Switch test
while counter < testLoops:
    print ('    Engage switch...')
    internalTimeout = axis1.calcTravelTimeForPosition(switchPosition)
    counter+=1
    if axis1.moveToSwitchBwd(internalTimeout):
        time.sleep(sleepTime)
        plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase+counter)
        #print(f'Test Number {testNumberBase+counter}')
    
    print (f'    Disengage switch (cycles = {counter})...')
    if axis1.moveRelativeAndWait(abs(stepSize)):
        time.sleep(sleepTime)
        plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase+counter+testLoops)
        #print(f'Test Number {testNumberBase+counter+testLoops}')
        

print(f'     Returning Bwd soft limt to initial state {bwdSoftLimitState}') 
if bwdSoftLimitState:
    axis1.setBwdSoftLimitsOn()

time.sleep(sleepTime)
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase)
print ('    Test Finished')