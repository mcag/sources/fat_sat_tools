#!/usr/bin/env python

import time
import sys
from motionFunctionsLib import *

########Internal variables########
counter = 0
defaultPLCport = 852
sleepTime = 1
##################################

if len(sys.argv)<7:
  print("ERROR: Missing arguments")  
  print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
  print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
  sys.exit()

try:
    plcNetId = sys.argv[1]
except:
    print('Error: Please specify the NetId of the motion controller')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    axisIdx = sys.argv[2]
except:
    print('Error: Please specify the Axis Index of the axis to test')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    testNumber = int(sys.argv[3]) # testNumberBaseFwd = 1000 and testNumberBaseBwd = 2000
except:
    print('Error: Missing test number')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    startPos = float(sys.argv[4])
except:
    print('Error: Please specify the limit starting position for the test')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    stopPos = float(sys.argv[5])
except:
    print('Error: Please specify the limit end position for the test')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    stepSize = float(sys.argv[6])
    if stepSize <= 0:
      print("Error: Step size has to be greater than 0")
      sys.exit() 
except:
    print('Error: Please specify the step size of the test')
    print("python ctrlTestStepScan.py <plcNetId> <axisIndex> <testNumber> <startPos> <stopPos> <stepSize> <velo=NC_default_vels(optional)> <plcPort=852>")
    print("python ctrlTestStepScan.py 172.30.41.144.1.1 1 1000 10 60 5 2 852")
    sys.exit()
try:
    velo = float(sys.argv[7])
    if velo == 0:
        print(f"Using TwinCAT NC configured velocity")
        velo = None
except:
    velo = None 
try:
    plcPort = int(sys.argv[8])
except:
    plcPort = defaultPLCport #Default PLC port for the tc_project_App is 852

print(f"    Connecting to {plcNetId} at port {plcPort} to GVL.astAxes[{axisIdx}]")

#PLC connection
plc1 = plc(plcNetId, plcPort)
axis1 = axis(plc1, axisIdx)

if not plc1.connection.is_open:
    for i in range(3):
        plc1.connect()
        time.sleep(sleepTime)
        i += 1
        if i == 3:
             print('    ERROR: PLC cannot connect')
             sys.exit()
        elif plc1.connection.is_open:
            print('   PLC connection successful')
            break

########Set initial conditions#######
pos = startPos

axis1.axisInit() #Disable, reset, enable



#Setting velocities
axis1.setJogVelocity(axis1.getAxisVeloManSlow())
axis1.setVelocity(axis1.getAxisVeloManFast())
if velo is not None: 
        axis1.setVelocity(velo)
        axis1.setJogVelocity(velo)

time.sleep(sleepTime)
print('    Test ready to begin...')

if startPos>stopPos:
    done = pos < stopPos
    countUp = False

if startPos<stopPos:
    done = pos > stopPos
    countUp = True

while not done:
  print (f'    move axis to position {pos}...')
  if not axis1.moveAbsoluteAndWait(pos):
    print ("    ERROR: Axis failed to position.")
    sys.exit()
  counter = counter + 1
  time.sleep(sleepTime)
  if countUp:
    plc1.connection.write_by_name('MAIN.nTestNum', testNumber + counter)
    print(f'    {testNumber + counter}')
  else:
    plc1.connection.write_by_name('MAIN.nTestNum', testNumber - counter)
    print(f'    {testNumber - counter}')

  if startPos>stopPos:
    pos = round(pos - stepSize,6)
    done = pos < stopPos
  if startPos<stopPos:    
    pos = round(pos + stepSize,6)
    done = pos > stopPos

time.sleep(sleepTime)
print ('    Step Scan Finished')
