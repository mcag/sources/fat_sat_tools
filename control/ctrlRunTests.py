#!/usr/bin/env python

import configparser
import os
import sys

##################################################################
LIMIT_STEP = 3
##########################################
#Read config.ini file
config = configparser.ConfigParser(inline_comment_prefixes="#")
config.read("cfgTests.ini")

#Get data from configuration file
##[General Configuration]
PLC_NETID = config.get('General Configuration','PLC_NETID')
MOTOR_GVL_INDEX = config.getint('General Configuration','MOTOR_GVL_INDEX')
TESTNUM_VAR = config.get('General Configuration','TESTNUM_VAR')
VELO = config.getfloat('General Configuration','VELO')
TIMEOUT = config.getint('General Configuration','TIMEOUT')
##[ISO230-2 Configuration]
ISO230_ENABLE = config.getboolean('ISO230-2 Configuration','ISO230_ENABLE')
ISO230_FIRST_TEST_POS = config.getfloat('ISO230-2 Configuration','ISO230_FIRST_TEST_POS')
ISO230_POS_STEP = config.getfloat('ISO230-2 Configuration','ISO230_POS_STEP')
ISO230_POS_COUNT = config.getint('ISO230-2 Configuration','ISO230_POS_COUNT')
ISO230_CYCLE_COUNT = config.getint('ISO230-2 Configuration','ISO230_CYCLE_COUNT')
ISO230_START_POS = config.getfloat('ISO230-2 Configuration','ISO230_START_POS')
ISO230_END_POS = config.getfloat('ISO230-2 Configuration','ISO230_END_POS')
##[Resolver Configuration]
RESOLVER_ENABLE = config.getboolean('Resolver Configuration','RESOLVER_ENABLE')
RESOLVER_TEST_POS = config.getfloat('Resolver Configuration','RESOLVER_TEST_POS')
RESOLVER_TEST_STEP = config.getfloat('Resolver Configuration','RESOLVER_TEST_STEP')
##[Limits Configuration]
LIMITS_ENABLE = config.getboolean('Limits Configuration','LIMITS_ENABLE')
##[Scan Configuration]
SCAN_ENABLE = config.getboolean('Scan Configuration','SCAN_ENABLE')

print(f"\nConnecting to motor with GVL index: {MOTOR_GVL_INDEX} of PLC with Net ID: {PLC_NETID}")

###Writing to TestNum variable 
print("\n0.0 Initialize tests...")
progCall = f"python ctrlTestInit.py {PLC_NETID} {MOTOR_GVL_INDEX}"
print(f'{progCall}')
os.system(progCall)


cmdLine = input("Ready to start the tests. Is the data logger running? (Y/N)")
if cmdLine == 'Y' or cmdLine == 'y':
  print('Data logger running, starting tests...')
else:
  print("Please start the data logger...")
  cmdLine = input("Is the data logger running already? (Y/N)")
  if cmdLine == 'Y' or cmdLine == 'y':
    print('Data logger running, starting tests...')
  else:
    print(f"Start the logger and run this script again")
    sys.exit()


#############################################################################
# ISO 230-2 test series
# Go to xx positions over stroke 5 times (allow max 99 positions otherwise testnumber will overflow)
# The testnumber is encoded like this <dir><cycle><position_2_dig>
#  <dir> = 1: Forward
#  <dir> = 2: Backward
#  <cycle> = 1..5 for the cycles
#  <position_2_dig> = 01..0x for the position

# A few examples:
# position 1101 is the first target position approached from below in test cycle 1
# position 2403 is the third target position approached from above in test cycle 4
#
# Cycle 1
# p forward  1101 1102 1103 1104 1105
# p backward 2101 2102 2103 2104 2105
# Cycle 2
# p forward  1201 1202 1203 1204 1205
# p backward 2201 2202 2203 2204 2205

# Call ISO230-2 test
if ISO230_ENABLE:
  print(f'\n1..2. Starting test according to ISO230-2 with {ISO230_POS_COUNT} position counts and {ISO230_CYCLE_COUNT} cycles in steps of {ISO230_POS_STEP}...')

  #Calculate last test position
  LAST_TEST_POS=ISO230_FIRST_TEST_POS+(ISO230_POS_COUNT-1)*ISO230_POS_STEP
  print(f"  Last test position is: {LAST_TEST_POS}...")

  for cycle in range(1,ISO230_CYCLE_COUNT+1):
    print(f"  Starting new cycle: {cycle}...")   

    # Go to start position
    print(f"  Go to start position {ISO230_START_POS}...")
    progCall = f"python ctrlTestMovePos.py {PLC_NETID} {MOTOR_GVL_INDEX} {ISO230_START_POS} {VELO}"
    print(f'{progCall}')
    os.system(progCall)

    TESTNUMBER= f"1{cycle}00" # testNumberBaseFwd = 1000 and testNumberBaseBwd = 2000

    print(f"  Start step scan forward... {ISO230_FIRST_TEST_POS} .. {LAST_TEST_POS} step {ISO230_POS_STEP}..")
  
    progCall = f"python ctrlTestStepScan.py {PLC_NETID} {MOTOR_GVL_INDEX} {TESTNUMBER} {ISO230_FIRST_TEST_POS} {LAST_TEST_POS} {ISO230_POS_STEP} {VELO}"
    print(f'{progCall}')
    os.system(progCall)

    # Go to end position
    print(f"  Go to end position...{ISO230_END_POS}...")
    progCall = f"python ctrlTestMovePos.py {PLC_NETID} {MOTOR_GVL_INDEX} {ISO230_END_POS} {VELO}"
    print(f'{progCall}')
    os.system(progCall)
    
    # Calc testnumber to correlate to forward positons in a better way
    TESTNUMBER=f"2{cycle}00" # testNumberBaseBwd = 2000
    TESTNUMBER= int(TESTNUMBER) + ISO230_POS_COUNT + 1
  
    print(f"  Start step scan backward... {LAST_TEST_POS}..{ISO230_FIRST_TEST_POS} step {ISO230_POS_STEP}..")
    progCall = f"python ctrlTestStepScan.py {PLC_NETID} {MOTOR_GVL_INDEX} {TESTNUMBER} {LAST_TEST_POS} {ISO230_FIRST_TEST_POS} {ISO230_POS_STEP} {VELO}"
    print(f'{progCall}')
    os.system(progCall)

  print ('    Test ISO230-2 Finished...')

if LIMITS_ENABLE:
  print('\n3. Starting backward limit jitter test..')
  progCall = f"python ctrlTestLimitBwd.py {PLC_NETID} {MOTOR_GVL_INDEX} {TIMEOUT} {VELO} {LIMIT_STEP}"
  print(f'{progCall}')
  os.system(progCall)

#Call Resolver test
if RESOLVER_ENABLE:
  print(f"Go to resolver test position... {RESOLVER_TEST_POS}...")
  progCall = f"python ctrlTestMovePos.py {PLC_NETID} {MOTOR_GVL_INDEX} {RESOLVER_TEST_POS} {VELO}"
  print(f'{progCall}')
  os.system(progCall)

  print(f'\n4. Starting resolver standstill jitter test at position {RESOLVER_TEST_POS} with steps of {RESOLVER_TEST_STEP}...')
  progCall = f"python ctrlTestResolver.py {PLC_NETID} {MOTOR_GVL_INDEX} {RESOLVER_TEST_STEP} {VELO}"
  print(f'{progCall}')
  os.system(progCall)

#Call Limit Swtches test
if LIMITS_ENABLE:
  print('\n5. Starting forward limit jitter test ...')
  progCall = f"python ctrlTestLimitFwd.py {PLC_NETID} {MOTOR_GVL_INDEX} {TIMEOUT} {VELO} {LIMIT_STEP}"
  print(f'{progCall}')
  os.system(progCall)
  

#Call Scan test
if SCAN_ENABLE:
  print('\n6. Jog backward to limit switch...')
  progCall = f"python ctrlTestScanBwd.py {PLC_NETID} {MOTOR_GVL_INDEX} {TIMEOUT} {VELO}"
  print(f'{progCall}')
  os.system(progCall)

  print('\n7. Jog forward to limit switch...')
  progCall = f"python ctrlTestScanFwd.py {PLC_NETID} {MOTOR_GVL_INDEX} {TIMEOUT} {VELO}"
  print(f'{progCall}')
  os.system(progCall)

print("\nTests finialized, please stop data acquisition...")