#!/usr/bin/env python

import time
import sys
from motionFunctionsLib import *

########Internal variables########
counter = 0
sleepTime = 1
testNumberBase = 6000
##################################

if len(sys.argv)<4:
  print("ERROR: Missing arguments: python ctrlTestScanBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <plcPort=852>")
  print("python ctrlTestScanBwd.py 172.30.41.144.1.1 1 10 2 852")
  sys.exit()

try:
    plcNetId = sys.argv[1]
except:
    print('Error: Please specify the NetId of the motion controller')
    print("python ctrlTestScanBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <plcPort=852>")
    print("python ctrlTestScanBwd.py 172.30.41.144.1.1 1 10 2 5 852")
    sys.exit()
try:
    axisIdx = sys.argv[2]
except:
    print('Error: Please specify the Axis Index of the axis to test')
    print("python ctrlTestScanBwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <plcPort=852>")
    print("python ctrlTestScanBwd.py 172.30.41.144.1.1 1 10 2 5 852")
    sys.exit()
try:
    timeout = int(sys.argv[3])
    if timeout <= 0:
       print('Error: Timeout value should be greater than 0')
       sys.exit()  
except:
    print('Error: Please specify a timeout value') 
    print("python ctrlTestLimitFwd.py <plcNetId> <axisIndex> <timeout> <velo=NC_default_vels> <stepSize=2> <plcPort=852>")
    print("python ctrlTestLimitFwd.py 172.30.41.144.1.1 1 10 100 2 5 852")
    sys.exit()
try:
    velo = float(sys.argv[4])
    if velo <= 0:
        print(f"Using TwinCAT NC configured velocity")
        velo = None
except:
    velo = None 
try:
    plcPort = int(sys.argv[5])
except:
    plcPort = 852 #Default PLC port for the tc_project_App is 852
    
print(f"  Connecting to {plcNetId} at port {plcPort} to GVL.astAxes[{axisIdx}]")

#PLC connection
plc1 = plc(plcNetId, plcPort)
axis1 = axis(plc1, axisIdx)

if not plc1.connection.is_open:
    for i in range(3):
        plc1.connect()
        time.sleep(sleepTime)
        i += 1
        if i == 3:
             print('    ERROR: PLC cannot connect')
             sys.exit()
        elif plc1.connection.is_open:
            print('PLC connection successful')
            break

########Set initial conditions#######
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase)

axis1.axisInit()

#Starting the test
print('     Initializing test prerequisites...')

########Set velocity#######
if velo is not None: 
    axis1.setJogVelocity(velo)
time.sleep(sleepTime)

bwdSoftLimitState = axis1.getSoftLimitBwdEnableStatus()
fwdSoftLimitState = axis1.getSoftLimitFwdEnableStatus()

print('     Moving to Fwd Limit swtich...')
axis1.moveToSwitchFwd(timeout)

time.sleep(sleepTime)

# Starting the scan backward
print('     Moving to Bwd Limit swtich...')
axis1.moveToSwitchBwd(timeout)

print(f'     Return soft limts to intial state Fwd = {fwdSoftLimitState} and Bwd = {bwdSoftLimitState}...')
if bwdSoftLimitState:
    axis1.setBwdSoftLimitsOn()
elif fwdSoftLimitState:
    axis1.setFwdSoftLimitsOn()

time.sleep(sleepTime)
plc1.connection.write_by_name('MAIN.nTestNum', testNumberBase+1) 
print ('    Now at switch')
