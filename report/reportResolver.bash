#!/bin/bash
# 

#*************************************************************************\
# Copyright (c) 2019 European Spallation Source ERIC
# ecmc is distributed subject to a Software License Agreement found
# in file LICENSE that is included with this distribution. 
#
#  reportResolver.bash
#
#  Created on: Oct 20, 2021
#      Author: anderssandstrom
#
#  Script for extracting and calculating statistics of resolver accuracy over one turn
#
#  Arguments:
#    1 Data file   (input)
#    2 Report file (output)
#    3 Test PV
#    4 Resolver PV
#    5 Resolver gain
#    6 Resolver offset
#    7 Motor Setpoint PV
#    8 Decimals
#    9 Unit
#
#*************************************************************************/

# Newline
nl='
'
if [ "$#" -ne 9 ]; then
   echo "reportResolver: Wrong arg count... Please specify input and output file."
   exit 1 
fi

FILE=$1
REPORT=$2
TEST_PV=$3
RESOLVER_PV=$4
RESOLVER_GAIN=$5
RESOLVER_OFFSET=$6
MOTORSET_PV=$7
DEC=$8
UNIT=$9

TESTBASE=4000

# Finds out what position by reading setpoint
DATACOUNT_RESOLVER="10"
TESTCOUNT=8

bash utilReport.bash $REPORT ""
bash utilReport.bash $REPORT "# Resolver Performance"
bash utilReport.bash $REPORT ""
bash utilReport.bash $REPORT " ## Configuration"
bash utilReport.bash $REPORT ""
bash utilReport.bash $REPORT "Setting | Value |"
bash utilReport.bash $REPORT "--- | --- |"
bash utilReport.bash $REPORT "Data file | $FILE |"
bash utilReport.bash $REPORT "Resolver position source  | $RESOLVER_PV |"
bash utilReport.bash $REPORT "Resolver gain  | $RESOLVER_GAIN |"
bash utilReport.bash $REPORT "Resolver offset  | $RESOLVER_OFFSET |"
bash utilReport.bash $REPORT "Target position source  | $MOTORSET_PV |"
bash utilReport.bash $REPORT "Test number source  | $TEST_PV |"
bash utilReport.bash $REPORT "Unit  | $UNIT |"
bash utilReport.bash $REPORT ""

# Resolver 
bash utilReport.bash $REPORT "## Resolver reading over one turn"
bash utilReport.bash $REPORT "Measured at $TESTCOUNT positions offset by 45deg resolver shaft angle."
bash utilReport.bash $REPORT "The distrubution values are based on $DATACOUNT_RESOLVER values at each location."
bash utilReport.bash $REPORT ""
bash utilReport.bash $REPORT "Test | Setpoint [$UNIT] | Resolver AVG[$UNIT] | Diff [$UNIT] | Resolver STD[$UNIT]"
bash utilReport.bash $REPORT "--- | --- | --- | --- | --- |"

TRIGGPV=$TEST_PV
DIFFS=""

for COUNTER in {1..8}
do
   # setpoint
   DATACOUNT="1"
   let "TRIGGVAL=$TESTBASE+$COUNTER"

   DATAPV=$MOTORSET_PV
   SETPOINT=$(bash utilGetDataBeforeTrigg.bash ${FILE} ${TRIGGPV} ${TRIGGVAL} ${DATAPV} ${DATACOUNT})
   echo "Setpoint=$SETPOINT"

   # resolver
   DATAPV=$RESOLVER_PV
   
   RESOLVER_VAL=$(bash utilGetLinesBeforeTrigg.bash ${FILE} ${TRIGGPV} ${TRIGGVAL} ${DATAPV} ${DATACOUNT_RESOLVER})
   RESOLVER_VAL=$(echo "$RESOLVER_VAL" | bash utilScaleOffsetLines.bash ${RESOLVER_GAIN} ${RESOLVER_OFFSET})
   RESOLVER_AVG=$(echo "$RESOLVER_VAL" | bash utilAvgLines.bash)
   RESOLVER_STD=$(echo "$RESOLVER_VAL" | bash utilStdLines.bash)
   DIFF_RESOLVER=$(echo "$RESOLVER_AVG $SETPOINT" | awk '{print $1-($2)}')
   DIFFS_RESOLVER+="$DIFF_RESOLVER "   
   echo "Resolver value AVG = $RESOLVER_AVG STD = $RESOLVER_STD"
   let "DEC_STD_AVG=$DEC+2"
   printf "%d | %.${DEC}f | %.${DEC_STD_AVG}f | %.${DEC_STD_AVG}f | %.${DEC_STD_AVG}f\n" $COUNTER $SETPOINT $RESOLVER_AVG $DIFF_RESOLVER $RESOLVER_STD >> $REPORT
done

MAX_RES_DIFF=$(echo "$DIFFS_RESOLVER" | bash utilAbsMaxDataRow.bash)
bash utilReport.bash $REPORT ""
printf "**Resolver accuracy: %.${DEC_STD_AVG}f [$UNIT]**\n" $MAX_RES_DIFF >> $REPORT
bash utilReport.bash $REPORT ""
