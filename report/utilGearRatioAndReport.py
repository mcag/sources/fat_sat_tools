#!/usr/bin/python
# coding: utf-8
import sys
import numpy as np
from utilISO230_2 import *

def printOutHelp():
  print ("python utilGearRatio.py <filename>]")
  print ("example: python utilGearRatio.py data.log")
  print ("example stdin: cat data.log | python utilGearRatio.py" )
  print ("Ouputs <gear_ratio> <offset> <datacount> <residual>")

def main():   
  if len(sys.argv ) > 2 :  
      printOutHelp()
      sys.exit()

  if len(sys.argv)==2:
    fname=sys.argv[1]

  if len(sys.argv)==1:
    fname=""
    
  iso = utilISO230_2()
  iso.loadFile(fname)
  iso.calcGearRatioAndISO230_2()
  
if __name__ == "__main__":
  main()
