#!/bin/bash
#*************************************************************************\
# Copyright (c) 2019 European Spallation Source ERIC
# ecmc is distributed subject to a Software License Agreement found
# in file LICENSE that is included with this distribution. 
#
#  reportMain.bash
#
#  Created on: Dec 14, 2020
#      Author: anderssandstrom
#
#
# Main script for processing data for IS=230-2 SAT/FAT.
#
# Arg 1 Cfg file   (input):
#    MOTORACT_PV            : Filter for motor open loop counter (actual position).
#    MOTORSET_PV            : Filter for motor setpint position (or target position).
#    RESOLVER_PV            : Filter fir resolver actrual postion.
#    RESOLVER_ENABLE        : Enable resolver test
#    REFERENCE_PV           : Filter for referance position.
#    TESTNUM_PV             : Filter for testnumber.
#    DEC                    : Decimals in printouts.
#    UNIT                   : Unit for the position measurement.
#    ISO230_POS_COUNT       : ISO230-2 cycle position count.
#    ISO230_CYCLE_COUNT     : ISO230-2 cycle count.
#    ISO230_ENABLE          : Enable ISO230-2 test cycle
#    LIMITS_ENABLE=1        : Enable Limit switch test
#    LIMITS_REF_POS_PV      : Data source for position during limit switch test
#    LIMITS_LOW_PV          : Data source PV for low limit switch
#    LIMITS_HIGH_PV         : Data source PV for high limit switch
#
# Arg 2 Data file   (input)
# Arg 3 Report file (output)
#
# NOTE: Varaibles below needs to modified for each case:
#
#*************************************************************************/
#
# Data examples (camonitor):
# IOC_TEST:m0s004-Enc01-PosAct   2021-10-27 09:04:48.529180 66.14321041  
# IOC_TEST:Axis1-PosAct          2021-10-27 09:04:48.529180 1.527265625  
# IOC_TEST:Axis1-PosSet          2021-10-27 09:04:48.529180 1.527243125  
# IOC_TEST:m0s004-Enc01-PosAct   2021-10-27 09:04:48.579143 66.12481403  
# IOC_TEST:m0s005-Enc01-PosAct   2021-10-27 09:04:48.579143 33.08044  
# IOC_TEST:Axis1-PosAct          2021-10-27 09:04:48.579143 1.564765625  
# IOC_TEST:Axis1-PosSet          2021-10-27 09:04:48.579143 1.564743125  
# IOC_TEST:m0s004-Enc01-PosAct   2021-10-27 09:04:48.629165 66.10466003  
# IOC_TEST:m0s005-Enc01-PosAct   2021-10-27 09:04:48.629165 2147.483643  
# IOC_TEST:Axis1-PosAct          2021-10-27 09:04:48.629165 1.602265625  

if [ "$#" -ne 3 ]; then
   echo "main: Wrong arg count... Please specify input and output file."
   exit 1 
fi

##########################################################################
# Below here NO variables should need to be updated/changed

CONFIG=$1
FILE=$2
REPORT=$3

# Execute cfg
source $CONFIG
# Calculate gearratios based on this test (should be correct defined)
TESTNUM_GEARRATIO_FROM=1501
TESTNUM_GEARRATIO_TO=2501

# Newline
nl='
'
# Always 5 cycles in standard
# Get forward direction data points (test numbers 1xx1..1xx)
TESTS=$(seq -w 1 1 $ISO230_POS_COUNT)
CYCLES=$(seq -w 1 1 $ISO230_CYCLE_COUNT)

## Init report file
bash utilReportInit.bash $REPORT $FILE

##### Gear Ratio ###################################################
# Use gear ratio python script to find gear ratios
echo "1. Calculate gear ratios..."
if [[ "$RESOLVER_CALC_GR" == "1" || "$RESOLVER_ENABLE" == "1" ]]; then
  # Get ISO230-2 RESOLVER data from input file for calc of gear ratio (Collect data with GR=1 and OFFSET=0)
  GEAR_RATIO_DATA_RESOLVER=$(bash utilGetISO230DataFromCAFile.bash $FILE $ISO230_CYCLE_COUNT $ISO230_POS_COUNT $RESOLVER_PV 1 0 $TESTNUM_PV $MOTORSET_PV $UNIT $DEC)
  #echo "$GEAR_RATIO_DATA_RESOLVER "
  TEMP=$( echo "$GEAR_RATIO_DATA_RESOLVER " | python utilGearRatio.py)
  RES_GR=$(echo $TEMP | awk '{print $1}')
  RES_OFF=$(echo $TEMP | awk '{print $2}')
  RES_LEN=$(echo $TEMP | awk '{print $3}')
  RES_ERR=$(echo $TEMP | awk '{print $4}')
  echo "RES GR=$RES_GR, OFF=$RES_OFF, LEN=$RES_LEN, RESIDUAL=$RES_ERR"
  RES_ERR_DISP=$RES_ERR
  RES_LEN_DISP=$RES_LEN
  RES_GR_DISP=$RES_GR
  RES_OFF_DISP=$RES_OFF
fi 
# Get ISO230-2 REFERENCE data from input file for calc of gear ratio (Collect data with GR=1 and OFFSET=0)
GEAR_RATIO_DATA_REFERENCE=$(bash utilGetISO230DataFromCAFile.bash $FILE $ISO230_CYCLE_COUNT $ISO230_POS_COUNT $REFERENCE_PV 1 0 $TESTNUM_PV $MOTORSET_PV $UNIT $DEC)
#echo "$GEAR_RATIO_DATA_REFERENCE "
TEMP=$( echo "$GEAR_RATIO_DATA_REFERENCE " | python utilGearRatio.py)
REF_GR=$(echo $TEMP | awk '{print $1}')
REF_OFF=$(echo $TEMP | awk '{print $2}')
REF_LEN=$(echo $TEMP | awk '{print $3}')
REF_ERR=$(echo $TEMP | awk '{print $4}')
echo "REF GR=$REF_GR, OFF=$REF_OFF, LEN=$REF_LEN, RESIDUAL=$REF_ERR"
REF_ERR_DISP=$REF_ERR
REF_LEN_DISP=$REF_LEN
REF_GR_DISP=$REF_GR
REF_OFF_DISP=$REF_OFF

# Report gear ratios
bash utilReport.bash $REPORT ""
bash utilReport.bash $REPORT "# Gear Ratios"
bash utilReport.bash $REPORT "From | To | Ratio [] | Offset [$UNIT] | Data count [] | Residual error [$UNIT²]"
bash utilReport.bash $REPORT "--- | --- | --- | --- | --- | --- |"
if (( $RESOLVER_ENABLE == 1 )) ; then
  bash utilReport.bash $REPORT "Target Position | Resolver | $RES_GR_DISP | $RES_OFF_DISP | $RES_LEN_DISP | $RES_ERR_DISP "
fi
bash utilReport.bash $REPORT "Target Position | Reference | $REF_GR_DISP | $REF_OFF_DISP | $REF_LEN_DISP | $REF_ERR_DISP "
bash utilReport.bash $REPORT ""

##### ISO230-2 ###################################################
if (( $ISO230_ENABLE == 1 )) ; then
  echo "2. ISO230-2 test..."
  # Use (correct gear ratio and offset)
  DONE=0
  if [ "$ISO230_REF_POS_PV" == "$RESOLVER_PV" ]; then
    ISO230_GR=$RES_GR
    ISO230_OFF=$RES_OFF
    DONE=1
  fi
  if [ "$ISO230_REF_POS_PV" == "$REFERENCE_PV" ]; then
    #The reference gear ratio is not applied, but if negative then data needs to change sign  
    if (( $(awk 'BEGIN{ print "'$REF_GR'"<"'0.0'" }') == 1 )); then
      ISO230_GR=-1
    else
      ISO230_GR=1
    fi 
    ISO230_OFF=$REF_OFF
    DONE=1
  fi
  if [ "$ISO230_REF_POS_PV" == "$MOTORACT_PV" ]; then
    ISO230_GR=1
    ISO230_OFF=0
    DONE=1
  fi

  # Check that calc is done
  if (( $DONE != 1 )) ; then
    echo "Error ISO230: No valid gear ratio and offset for $ISO230_REF_POS_PV..."
    exit
  fi

  # Get ISO230-2 data from input file but with correct gear ratios and offset
  TEST_DATA=$(bash utilGetISO230DataFromCAFile.bash $FILE $ISO230_CYCLE_COUNT $ISO230_POS_COUNT $ISO230_REF_POS_PV $ISO230_GR $ISO230_OFF $TESTNUM_PV $MOTORSET_PV $UNIT $DEC)
  # Save to temp file for fun..
  ISO230_2_FILE="${REPORT}_ISO230.dat"
  echo "TEST_DATA=$TEST_DATA " > $ISO230_2_FILE
  # Calc ISO230-2 data
  ISO230_OUTPUT=$( echo "$TEST_DATA " | python utilISO230_2.py)
  
  # Add output from python to report
  echo "$ISO230_OUTPUT " >> $REPORT
  bash utilReport.bash $REPORT ""
fi

##### Switches ##########################################################
if (( $LIMITS_ENABLE == 1 )) ; then
  DONE=0
  echo "3. Limit switch test..."
  # Use (correct gear ratio and offset)
  if [ "$LIMITS_REF_POS_PV" == "$RESOLVER_PV" ]; then
    LIM_GR=$RES_GR
    LIM_OFF=$RES_OFF
    DONE=1
  fi
  if [ "$LIMITS_REF_POS_PV" == "$REFERENCE_PV" ]; then
      #The reference gear ratio is not applied, but if negative then data needs to change sign  
    if (( $(awk 'BEGIN{ print "'$REF_GR'"<"'0.0'" }') == 1 )); then
      LIM_GR=-1
    else
      LIM_GR=1
    fi 
    LIM_OFF=$REF_OFF
    DONE=1
  fi
  if [ "$LIMITS_REF_POS_PV" == "$MOTORACT_PV" ]; then
    LIM_GR=1
    LIM_OFF=0
    DONE=1
  fi
  # Check that calc is done
  if (( $DONE != 1 )) ; then
    echo "Error Limits: No valid gear ratio and offset for $LIMITS_REF_POS_PV..."
    exit
  fi

  bash reportSwitch.bash $FILE $REPORT $TESTNUM_PV $LIMITS_REF_POS_PV $LIM_GR $LIM_OFF $LIMITS_LOW_PV $LIMITS_HIGH_PV $DEC $UNIT    
  
fi

##### Resolver jitter ###################################################
if (( $RESOLVER_ENABLE == 1 )) ; then
  echo "4. Resolver performance test..."
  bash reportResolver.bash $FILE $REPORT $TESTNUM_PV $RESOLVER_PV $RES_GR $RES_OFF $MOTORSET_PV $DEC $UNIT
fi
##### Finish ###################################################
echo "5. Report ready..."

# Todo: analyze scan SCAN_ENABLE
